package com.company;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        StartConvert(in);


    }

    private static void StartConvert(Scanner in) {
        String s = in.nextLine();
        char[][] c = {{'.', '!', '?'}, {'a', 'b', 'c'}, {'d', 'e', 'f'}
                    , {'g', 'h', 'i'}, {'j', 'k', 'l'}, {'m', 'n', 'o'}
                    , {'p', 'q', 'r'}, {'s', 't', 'u'}, {'v', 'w', 'x'},
                      {'y', 'z', ' '}};
        keyPadOperation(s, c);
    }

    private static void keyPadOperation(String s, char[][] c) {

        for (int k = 0; k < s.length(); k++) {
            int count=0;
            for (int i = 0; i < c.length; i++) {
                for (int j = 0; j < c[j].length; j++) {

                    if (s.charAt(k) == c[i][j]) {

                        while (count <= j) {
                            if (i == 9) {
                                System.out.print("0");
                            }else {
                                System.out.print(i + 1);
                            }
                        count++;
                        }
                    }
                }

            }
        }
    }

}
